# TweetSim

# How do I run this project on my machine?

## 1. Clone the repository.

	git clone https://donaldtyson@bitbucket.org/donaldtyson/tweetsim.git
	
##2. In the project directory, run the Gradle Wrapper script.

On Windows:
	
	gradlew.bat clean test run --args='user.txt tweet.txt'
		
On Linux or macOS:
	
	./gradlew clean test run --args='user.txt tweet.txt'

# How do I provide my own user.txt and tweet.txt?

Replace the `user.txt` and the `tweet.txt` files in the project directory with files of the same name.

You can also provide the absolute system path to each file in the gradle command.

# Sample output:

```
> Task :test

feed.FeedManagerTest > shouldFormatUsersFeed PASSED

feed.FeedManagerTest > shouldSayIfTweetIsByUsersFollowees PASSED

feed.FeedManagerTest > shouldValidateUserExists PASSED

feed.FeedManagerTest > shouldGetMartinsUserFeed PASSED

feed.FeedManagerTest > shouldThrowExceptionForNonExistentUser PASSED

feed.FeedManagerTest > shouldSayIfTweetIsByUser PASSED

feed.FeedManagerTest > shouldGetWardsUserFeed PASSED

feed.FeedManagerTest > shouldMakeErrorMessage PASSED

users.UserManagerTest > shouldGetUserWithFollowees PASSED

users.UserManagerTest > shouldCreateMalformedMessage PASSED

users.UserManagerTest > shouldGetUserFromLine PASSED

users.UserManagerTest > shouldDiscoverAllUsers PASSED

users.UserManagerTest > shouldFindUserByName PASSED

users.UserManagerTest > shouldValidateUserFile PASSED

users.UserManagerTest > shouldReadUserFile PASSED

users.UserManagerTest > shouldUpdateFollowees PASSED

users.UserManagerTest > shouldGetFolloweesFromLine PASSED

users.UserManagerTest > shouldReturnUsersInAlphabeticalOrderByName PASSED

tweets.TweetManagerTest > shouldCreateMalformedMessage PASSED

tweets.TweetManagerTest > shouldGetUserFromLine PASSED

tweets.TweetManagerTest > shouldValidateTweetFileFormat PASSED

tweets.TweetManagerTest > shouldGetTweets PASSED

tweets.TweetManagerTest > shouldValidateTweetCharacterCount PASSED

tweets.TweetManagerTest > shouldReadTweetFile PASSED

tweets.TweetManagerTest > shouldGetTweetTextFromLine PASSED

> Task :run
Alan
        @Alan: If you have a procedure with 10 parameters, you probably missed some.
        @Alan: Random numbers should not be generated with a method chosen at random.

Martin

Ward
        @Alan: If you have a procedure with 10 parameters, you probably missed some.
        @Ward: There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.
        @Alan: Random numbers should not be generated with a method chosen at random.
```