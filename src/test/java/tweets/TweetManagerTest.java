package tweets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import testutils.FileUtils;

public class TweetManagerTest {
	
	private final String testFile = System.getProperty("java.io.tmpdir")+"/tweet.txt";

	private final String testFileContent = "Alan> If you have a procedure with 10 parameters, you probably missed some.%n" + 
			"Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.%n" + 
			"Alan> Random numbers should not be generated with a method chosen at random.";
	
	private TweetManager tweetManager;
	
	@Before
	public void setup() {
		tweetManager = new TweetManager();
		FileUtils.createTestFile(testFile, testFileContent);
	}
	
	@Test
	public void shouldReadTweetFile() {
		StringBuilder builder = new StringBuilder();
		Stream<String> lines = tweetManager.readTweetFile(testFile);
		lines.forEach(line->builder.append(line));
		assertEquals(testFileContent, builder.toString());
	}
	
	@Test
	public void shouldGetUserFromLine() {
		String tweetLine = "Alan> If you have a procedure with 10 parameters, you probably missed some.";
		String userName = tweetManager.parseUserName(tweetLine);
		assertEquals("Alan", userName);
	}
	
	@Test
	public void shouldGetTweetTextFromLine() {
		String tweetLine = "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors. ";
		String expected = "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors. ";
		String tweetContent = tweetManager.parseTweetText(tweetLine);
		assertEquals(expected, tweetContent);
	}
	
	@Test
	public void shouldValidateTweetFileFormat() {
		String correctFormat = testFileContent;
		String incorrecFormat = "<Intentionally Malformed Tweet Test Line> Alan>>>>Some text." + System.lineSeparator() +
				"<Intentionally Malformed Tweet Test Line> <<>>>Some text.";
		assertFalse(tweetManager.isValidTweetFile(toStream(incorrecFormat)));
		assertTrue(tweetManager.isValidTweetFile(toStream(correctFormat)));
	}
	
	@Test
	public void shouldValidateTweetCharacterCount() {
		String invalidTweet = IntStream.rangeClosed(0, 140)
				.mapToObj(i->"a").collect(Collectors.joining());
		assertFalse(tweetManager.isValidTweet(invalidTweet));
	}
	
	@Test
	public void shouldGetTweets() {
		List<Tweet> parsedTweets = tweetManager.parseTweets(toStream(testFileContent));
		assertEquals(3, parsedTweets.size());
		assertTrue(parsedTweets.containsAll(listOfTestTweets()));
	}
	
	private ArrayList<Tweet> listOfTestTweets() {
		return new ArrayList<Tweet>() {{
			add(new Tweet("Alan", "If you have a procedure with 10 parameters, you probably missed some."));
			add(new Tweet("Ward", "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."));
			add(new Tweet("Alan", "Random numbers should not be generated with a method chosen at random."));
		}};
	}
	
	@Test
	public void shouldCreateMalformedMessage() {
		String expected = "Tweet file malformed at the following lines :" + System.lineSeparator() +
			"error";
		List<String> errorLines = new ArrayList<String>() {{ add("error"); }};
		assertEquals(expected, tweetManager.makeLineMalformedErrorMessage(errorLines));
	}
	
	private Stream<String> toStream(String content) {
		return Stream.of(content.split("%n"));
	}

	@After
	public void tearDown() {
		FileUtils.deleteTestFile(testFile);
	}
}
