package testutils;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtils {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);
	
	public static void createTestFile(String fileName, String fileContent) {
		try {
			Files.write(Paths.get(fileName), fileContent.getBytes());
		} catch (IOException e) {
			LOGGER.error("Could not create test file " + fileName + ". " + e.getMessage());
		}
	}
	
	public static void deleteTestFile(String fileName) {
		try {
			Files.delete(Paths.get(fileName));
		} catch (IOException e) {
			LOGGER.error("Could not delete test file " + fileName + ". " + e.getMessage());
		}
	}
}
