package users;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import testutils.FileUtils;

public class UserManagerTest {
	
	private final String MARTIN = "Martin";
	private final String ALAN = "Alan";
	private final String WARD = "Ward";
	
	private final String testFile = System.getProperty("java.io.tmpdir")+"/user.txt";
	
	private final String testFileContent = "Ward follows Alan%n" + 
			"Alan follows Martin%n" + 
			"Ward follows Martin, Alan";
	
	private UserManager userManager;
	
	@Before
	public void setup() {
		userManager = new UserManager();
		FileUtils.createTestFile(testFile, testFileContent);
	}
	
	@Test
	public void shouldReadUserFile() {
		StringBuilder builder = new StringBuilder();
		Stream<String> lines = userManager.readUserFile(testFile);
		lines.forEach(line->builder.append(line));
		assertEquals(testFileContent, builder.toString());
	}
	
	@Test
	public void shouldGetUserFromLine() {
		String userLine = "Ward follows Martin, Alan";
		String userName = userManager.parseUserName(userLine);
		assertEquals(WARD, userName);
	}
	
	@Test
	public void shouldGetFolloweesFromLine() {
		String userLine = "Ward follows Martin, Alan";
		List<String> followees = userManager.parseFollowees(userLine);
		ensureFolloweesAreFound(followees);
	}
	
	private void ensureFolloweesAreFound(List<String> followees) {
		assertEquals(2, followees.size());
		assertFalse(followees.stream().anyMatch(f->f.equals(WARD)));
		assertTrue(followees.contains(MARTIN));
		assertTrue(followees.contains(ALAN));
	}
	
	@Test
	public void shouldFindUserByName() {
		final String name = WARD;
		SortedSet<User> users = new TreeSet<User>(Arrays.asList(new User(name)));
		User user = userManager.findByName(users, name);
		assertEquals(name, user.getName());
	}
	
	@Test
	public void shouldUpdateFollowees() {
		final String userLine = "Ward follows Martin, Alan";
		List<String> followees = userManager.parseFollowees(userLine);
		User user = new User(WARD);
		userManager.updateFollowees(user, followees);
		ensureFolloweesAreUpdated(user);
	}
	
	private void ensureFolloweesAreUpdated(User user) {
		assertEquals(2, user.getFollowees().size());
		assertTrue(user.getFollowees().contains(MARTIN));
		assertTrue(user.getFollowees().contains(ALAN));
	}
	
	@Test
	public void shouldDiscoverAllUsers() {
		final String userLine = "Ward follows Martin, Alan";
		SortedSet<User> users = new TreeSet<User>(Arrays.asList(new User(WARD)));
		userManager.updateUsersFromFollowees(users, userManager.parseFollowees(userLine));
		ensureAllUsersAreDiscovered(users);
	}
	
	private void ensureAllUsersAreDiscovered(SortedSet<User> users) {
		assertEquals(3, users.size());
		assertTrue(users.stream().anyMatch(f->f.getName().equals(MARTIN)));
		assertTrue(users.stream().anyMatch(f->f.getName().equals(ALAN)));
		assertTrue(users.stream().anyMatch(f->f.getName().equals(WARD)));
	}
	
	@Test
	public void shouldGetUserWithFollowees() {
		SortedSet<User> users = userManager.parseUserFileContent(toStream(testFileContent));
		assertEquals(3, users.size());
		assertEquals(2, users.stream().filter(u->u.getName().equals(WARD)).findFirst().get().getFollowees().size());
	}
	
	@Test
	public void shouldValidateUserFile() {
		String correctFormat = testFileContent;
		String incorrectFormat = "<Intentionally Malformed User Test Line> Alan can follow Martin, SomeUser" + System.lineSeparator()
				+ "<Intentionally Malformed User Test Line> incorrectly formatted file";
		assertFalse(userManager.isValidUserFile(toStream(incorrectFormat)));
		assertTrue(userManager.isValidUserFile(toStream(correctFormat)));
	}
	
	@Test
	public void shouldCreateMalformedMessage() {
		String expected = "User file malformed at the following lines :" + System.lineSeparator() +
			"error";
		List<String> errorLines = new ArrayList<String>() {{ add("error"); }};
		assertEquals(expected, userManager.makeLineMalformedErrorMessage(errorLines));
	}
	
	@Test
	public void shouldReturnUsersInAlphabeticalOrderByName() {
		SortedSet<User> users = userManager.parseUserFileContent(toStream(testFileContent));
		assertEquals(WARD, users.last().getName());
		assertEquals(ALAN,users.first().getName());
	}
	
	private Stream<String> toStream(String string) {
		return Stream.of(string.split("%n"));
	}
	
	@After
	public void tearDown() {
		FileUtils.deleteTestFile(testFile);
	}
}
