package feed;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import tweets.Tweet;
import users.User;

public class FeedManagerTest {
	
	private FeedManager feedManager;
	
	@Before
	public void setup() {
		feedManager = new FeedManager(listOfTestTweets(), listOfTestUsers());
	}
	
	private User testUserWard() {
		User ward = new User("Ward");
		List<String> wardFollows = new ArrayList<String>();
		wardFollows.add("Alan");
		wardFollows.add("Martin");
		ward.setFollowees(wardFollows);
		return ward;
	}
	
	private User testUserAlan() {
		User alan = new User("Alan");
		List<String> alanFollows = new ArrayList<String>();
		alanFollows.add("Martin");
		alan.setFollowees(alanFollows);
		return alan;
	}
	
	private User testUserMartin() {
		User martin = new User("Martin");
		List<String> martinFollows = new ArrayList<String>();
		martin.setFollowees(martinFollows);
		return martin;
	}
	
	private TreeSet<User> listOfTestUsers() {
		return new TreeSet<User>() {{
			add(testUserAlan());
			add(testUserWard());
			add(testUserMartin());
		}};
	}
	
	private ArrayList<Tweet> listOfTestTweets() {
		return new ArrayList<Tweet>() {{
			add(new Tweet("Alan", "If you have a procedure with 10 parameters, you probably missed some."));
			add(new Tweet("Ward", "There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors."));
			add(new Tweet("Alan", "Random numbers should not be generated with a method chosen at random."));
		}};
	}
	
	@Test
	public void shouldValidateUserExists() {
		assertFalse(feedManager.userExists("Thomas"));
	}
	
	@Test
	public void shouldMakeErrorMessage() {
		String expected = "Thomas does not exist in user set.";
		String message = feedManager.makeErrorMessage("Thomas");
		assertEquals(expected, message);
	}
	
	@Test
	public void shouldSayIfTweetIsByUsersFollowees() {
		User user = testUserWard();
		Tweet tweet1 = new Tweet("Alan", "some text");
		Tweet tweet2 = new Tweet("Mike", "some text");
		assertTrue(feedManager.isTweetByOneOfUsersFollowees(tweet1, user.getName()));
		assertFalse(feedManager.isTweetByOneOfUsersFollowees(tweet2, user.getName()));
	}
	
	@Test
	public void shouldSayIfTweetIsByUser() {
		User user = testUserMartin();
		Tweet tweet = new Tweet("Martin", "some text");
		assertTrue(feedManager.isTweetByUser(tweet, user.getName()));
		assertFalse(feedManager.isTweetByUser(tweet, "Alan"));
	}
	
	@Test
	public void shouldGetWardsUserFeed() throws Exception {
		List<Tweet> userTweets = feedManager.getTweetFeedForUser("Ward");
		assertEquals(listOfTestTweets(), userTweets);
	}
	
	@Test
	public void shouldGetMartinsUserFeed() throws Exception {
		List<Tweet> userTweets = feedManager.getTweetFeedForUser("Martin");
		assertEquals(new ArrayList<Tweet>(), userTweets);
	}
	
	@Test(expected = Exception.class)
	public void shouldThrowExceptionForNonExistentUser() throws Exception {
		feedManager.getTweetFeedForUser("William");
	}
	
	@Test
	public void shouldFormatUsersFeed() {
		String expected = "Ward" + System.lineSeparator() +
				"\t@Alan: If you have a procedure with 10 parameters, you probably missed some." + System.lineSeparator() +
				"\t@Ward: There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors." + System.lineSeparator() +
				"\t@Alan: Random numbers should not be generated with a method chosen at random." + System.lineSeparator();
		List<Tweet> feed = listOfTestTweets();
		String user = "Ward";
		String formattedFeed = feedManager.formatUserFeed(user, feed);
		assertEquals(expected, formattedFeed);
	}
}
