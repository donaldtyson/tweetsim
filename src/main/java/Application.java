import java.util.List;
import java.util.SortedSet;
import java.util.function.Supplier;
import java.util.stream.Stream;

import feed.FeedManager;
import tweets.Tweet;
import tweets.TweetManager;
import users.User;
import users.UserManager;

public class Application {
	public static void main(String[] args) throws Exception {
		
		validateArguments(args);
		
		String userFile = args[0];
		String tweetFile = args[1];
		
		UserManager userManager = new UserManager();
		TweetManager tweetManager = new TweetManager();
		FeedManager feedManager;
		
		Supplier<Stream<String>> userFileContent = () -> userManager.readUserFile(userFile);
		Supplier<Stream<String>> tweetFileContent = () -> tweetManager.readTweetFile(tweetFile);
		
		SortedSet<User> users;
		if(userManager.isValidUserFile(userFileContent.get()))
			users = userManager.parseUserFileContent(userFileContent.get());
		else return;
		
		List<Tweet> tweets;
		if(tweetManager.isValidTweetFile(tweetFileContent.get()))
			tweets = tweetManager.parseTweets(tweetFileContent.get());
		else return;
		
		feedManager = new FeedManager(tweets, users);
		feedManager.printFeed();
	}
	
	private static void validateArguments(String[] arguments) throws Exception {
		if(arguments.length<2) 
			throw new Exception("Please provide a user file and a tweet file as arguments.");
	}
}
