package tweets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TweetManager {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(TweetManager.class);

	public Stream<String> readTweetFile(String absolutePath) {
		try {
			return Files.lines(Paths.get(absolutePath));
		} catch (IOException e) {
			LOGGER.error("Could not read tweet file " + absolutePath + ". " + e.getMessage());
		}
		return null;
	}

	public String parseUserName(String tweetLine) {
		return tweetLine.split("> ")[0];
	}

	public String parseTweetText(String tweetLine) {
		return tweetLine.split("> ")[1];
	}

	public boolean isValidTweetFile(Stream<String> lines) {
		List<String> errorLines = new ArrayList<String>();
		lines.forEach(line->{
			String[] Usertweet = line.split("> ");
			if(Usertweet.length!=2 || !isValidTweet(Usertweet[1])) errorLines.add(line);
		});
		if(errorLines.isEmpty()) return true;
		else LOGGER.error(makeLineMalformedErrorMessage(errorLines));
		return false;
	}
	
	public String makeLineMalformedErrorMessage(List<String> errorLines) {
		return "Tweet file malformed at the following lines :" + System.lineSeparator() +
				errorLines.stream().map(s->s).collect(Collectors.joining(System.lineSeparator()));
	}

	public List<Tweet> parseTweets(Stream<String> lines) {
		List<Tweet> tweets = new ArrayList<Tweet>();
		lines.forEach(line->{
			tweets.add(new Tweet(parseUserName(line), parseTweetText(line)));
		});
		return tweets;
	}

	public boolean isValidTweet(String tweet) {
		return tweet.length()<=140;
	}
}
