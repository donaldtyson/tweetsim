package feed;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import tweets.Tweet;
import users.User;
import users.UserManager;

public class FeedManager {
	
	List<Tweet> tweets;
	
	SortedSet<User> users = new TreeSet<User>();
	
	UserManager userManager = new UserManager();
	
	public FeedManager(List<Tweet> tweets, SortedSet<User> users) {
		this.tweets = tweets;
		this.users = users;
	}

	public List<Tweet> getTweetFeedForUser(String userName) throws Exception {
		if(!userExists(userName)) throw new Exception(makeErrorMessage(userName));
		List<Tweet> feed = new ArrayList<Tweet>();
		for(Tweet tweet : tweets) {
			if(isTweetByUser(tweet, userName) || isTweetByOneOfUsersFollowees(tweet, userName))
				feed.add(tweet);
		}
		return feed;
	}
	
	public boolean isTweetByOneOfUsersFollowees(Tweet tweet, String userName) {
		User user = userManager.findByName(users, userName);
		return user.getFollowees().contains(tweet.getUser());
	}
	
	public boolean isTweetByUser(Tweet tweet, String userName) {
		return tweet.getUser().equals(userName);
	}

	public boolean userExists(String userName) {
		return this.users.stream().anyMatch(u->u.getName().equals(userName));
	}

	public String makeErrorMessage(String userName) {
		return userName + " does not exist in user set.";
	}

	public String formatUserFeed(String userName, List<Tweet> feed) {
		StringBuilder builder = new StringBuilder();
		builder.append(userName + System.lineSeparator());
		for(Tweet tweet : feed) {
			builder.append("\t@")
			.append(tweet.getUser())
			.append(": ")
			.append(tweet.getContent())
			.append(System.lineSeparator());
		}
		return builder.toString();
	}
	
	public void printFeed() throws Exception {
		for(User user : users) {
			System.out.println(this.formatUserFeed(user.getName(), this.getTweetFeedForUser(user.getName())));
		}
	}
}
