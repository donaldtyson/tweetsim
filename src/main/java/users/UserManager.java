package users;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserManager {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(UserManager.class);
	
	public Stream<String> readUserFile(String absolutePath) {
		try {
			return Files.lines(Paths.get(absolutePath));
		} catch (IOException e) {
			LOGGER.error("Could not read user file " + absolutePath + ". " + e.getMessage());
		}
		return null;
	}

	public String parseUserName(String userLine) {
		return userLine.split(" follows ")[0];
	}

	public List<String> parseFollowees(String userLine) {
		List<String> followees = new ArrayList<String>();
		String[] followsSplit = userLine.split(" follows ");
		String[] followeesNames = followsSplit[1].replaceAll(" ", "").split(",");
		Arrays.asList(followeesNames).forEach(f->followees.add(f));
		return followees;
	}
	
	public void updateFollowees(User user, List<String> followees) {
		followees.stream().forEach(followee->{
			List<String> existingFollowees = user.getFollowees();
			if(!user.getFollowees().contains(followee))
				existingFollowees.add(followee);
		});
	}

	public boolean userExists(SortedSet<User> users, String name) {
		return users.stream().anyMatch(user->user.getName().equals(name));
	}
	
	public User findByName(SortedSet<User> users, String name) {
		Optional<User> user = users.stream().filter(u->u.getName().equals(name)).findFirst();
		if(user.isPresent()) return user.get();
		return null;
	}

	public void updateUsersFromFollowees(SortedSet<User> users, List<String> parseFollowees) {
		for(String followee : parseFollowees) {
			if(!userExists(users, followee))
				users.add(new User(followee));
		}
	}

	public SortedSet<User> parseUserFileContent(Stream<String> userFile) {
		SortedSet<User> users = new TreeSet<User>();
		userFile.forEach(line->{
			String userName = parseUserName(line);
			List<String> followees = parseFollowees(line);
			updateUsersFromFollowees(users, followees);
			if(!userExists(users, userName)) users.add(new User(userName, followees));
			else updateFollowees(findByName(users, userName), followees);
		});
		return users;
	}
	
	public boolean isValidUserFile(Stream<String> lines) {
		List<String> errorLines = new ArrayList<String>();
		lines.forEach(line->{
			if(line.split(" follows ").length!=2) errorLines.add(line);
		});
		if(errorLines.isEmpty()) return true;
		else LOGGER.error(makeLineMalformedErrorMessage(errorLines));
		return false;
	}
	
	public String makeLineMalformedErrorMessage(List<String> errorLines) {
		return "User file malformed at the following lines :" + System.lineSeparator() +
				errorLines.stream().map(s->s).collect(Collectors.joining(System.lineSeparator()));
	}
}
	
