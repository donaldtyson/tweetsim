package users;
import java.util.ArrayList;
import java.util.List;

public class User implements Comparable<User>{
	
	String name;
	
	List<String> followees = new ArrayList<String>();
	
	public User(String name, List<String> followees) {
		this.name = name;
		this.followees = followees;
	}
	
	public User(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public List<String> getFollowees() {
		return followees;
	}

	public void setFollowees(List<String> followees) {
		this.followees = followees;
	}

	@Override
	public int compareTo(User o) {
		return this.name.compareTo(o.getName());
	}
}
